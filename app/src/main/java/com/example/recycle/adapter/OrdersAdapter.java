package com.example.recycle.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recycle.R;
import com.example.recycle.model.my_orders.MyOrdersModel;

import java.util.List;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    List<MyOrdersModel> myOrdersModels;
    ItemListener onClickListener;

    public OrdersAdapter(List<MyOrdersModel> myOrdersModels, ItemListener onClickListener) {
        this.myOrdersModels = myOrdersModels;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public OrdersAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.my_orders_shap,parent,false);
        OrdersAdapter.ViewHolder vh=new OrdersAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull OrdersAdapter.ViewHolder holder, int position) {
        holder.itemName.setText(myOrdersModels.get(position).getItemName());
        holder.date.setText(myOrdersModels.get(position).getDate());
        holder.price.setText(myOrdersModels.get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return myOrdersModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView itemName;
        TextView date;
        TextView price;
        ImageButton delete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName=itemView.findViewById(R.id.ordersItemName);
            date=itemView.findViewById(R.id.ordersItemDate);
            price=itemView.findViewById(R.id.ordersItemPrice);
            delete=itemView.findViewById(R.id.deleteOrder);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListener.onDeleteClick(view,getAdapterPosition());
                }
            });
        }
    }
    public interface ItemListener {
        public void onDeleteClick(View v,int position);
    }
}
