package com.example.recycle.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recycle.R;
import com.example.recycle.model.item_of_category.ItemOfCategoryModel;
import com.example.recycle.model.search_model.SearchModel;

import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    List<SearchModel> searchModels;
    ItemListener itemListener;

    public SearchAdapter(List<SearchModel> searchModels, ItemListener itemListener) {
        this.searchModels = searchModels;
        this.itemListener = itemListener;
    }

    @NonNull
    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.search_shap,parent,false);
        SearchAdapter.ViewHolder viewHolder=new SearchAdapter.ViewHolder(v,itemListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchAdapter.ViewHolder holder, int position) {
        holder.itemName.setText(searchModels.get(position).getItemName());
        holder.itemPrice.setText(searchModels.get(position).getPrice());
        holder.itemDate.setText(searchModels.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return searchModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView itemName;
        TextView itemPrice;
        TextView itemDate;
        ItemListener itemListener;
        public ViewHolder(@NonNull View itemView,ItemListener itemListener) {
            super(itemView);
            itemName=itemView.findViewById(R.id.searchedItemName);
            itemPrice=itemView.findViewById(R.id.searchedItemPrice);
            itemDate=itemView.findViewById(R.id.searchedDate);
            this.itemListener=itemListener;
            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            itemListener.onClick(getAdapterPosition());
        }
    }
    public interface ItemListener{
        void onClick(int position);
    }
}
