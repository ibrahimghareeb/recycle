package com.example.recycle.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recycle.R;
import com.example.recycle.model.request.RequestModel;

import java.util.List;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {

    List<RequestModel> requestModels;
    public ItemListener onClickListner;

    public RequestAdapter(List<RequestModel> requestModels, ItemListener onClickListner) {
        this.requestModels = requestModels;
        this.onClickListner = onClickListner;
    }

    @NonNull
    @Override
    public RequestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.request_shape,parent,false);
        RequestAdapter.ViewHolder vh=new RequestAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RequestAdapter.ViewHolder holder, int position) {
        holder.itemName.setText(requestModels.get(position).getItemName());
        holder.nameCompany.setText(requestModels.get(position).getNameCompany());
        holder.price.setText(requestModels.get(position).getPrice());
        holder.quantity.setText(String.valueOf(requestModels.get(position).getQuantity()));
    }

    @Override
    public int getItemCount() {
        return requestModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView itemName;
        TextView nameCompany;
        TextView price;
        TextView quantity;
        ImageButton accept;
        ImageButton refuse;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName=itemView.findViewById(R.id.requestedItemName);
            nameCompany=itemView.findViewById(R.id.requestedItemNameCompany);
            price=itemView.findViewById(R.id.requestedItemPrice);
            accept=itemView.findViewById(R.id.acceptRequest);
            refuse=itemView.findViewById(R.id.refuseRequest);
            quantity=itemView.findViewById(R.id.requestedQuantity);
            accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListner.onAcceptClick(view,getAdapterPosition());
                }
            });
            refuse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListner.onRefuseClick(view,getAdapterPosition());
                }
            });
        }
    }

    public interface ItemListener {
        public void onAcceptClick(View v,int position);
        public void onRefuseClick(View v,int position);
    }
}
