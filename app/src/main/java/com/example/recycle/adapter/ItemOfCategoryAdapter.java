package com.example.recycle.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recycle.R;
import com.example.recycle.model.item_of_category.ItemOfCategoryModel;
import com.example.recycle.ui.item_of_category.ItemOfCategory;

import java.util.List;

public class ItemOfCategoryAdapter extends RecyclerView.Adapter<ItemOfCategoryAdapter.ViewHolder> {
    List<ItemOfCategoryModel> itemOfCategories;
    ItemListener itemListener;

    public ItemOfCategoryAdapter(List<ItemOfCategoryModel> itemOfCategories, ItemListener itemListener) {
        this.itemOfCategories = itemOfCategories;
        this.itemListener = itemListener;
    }

    @NonNull
    @Override
    public ItemOfCategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_of_catgory_shape,parent,false);
        ItemOfCategoryAdapter.ViewHolder viewHolder=new ItemOfCategoryAdapter.ViewHolder(v,itemListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemOfCategoryAdapter.ViewHolder holder, int position) {
        holder.itemName.setText(itemOfCategories.get(position).getItemName());
        holder.itemPrice.setText(itemOfCategories.get(position).getPrice());
        holder.itemDate.setText(itemOfCategories.get(position).getDate());
    }

    @Override
    public int getItemCount() {
        return itemOfCategories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView itemName;
        TextView itemPrice;
        TextView itemDate;
        ItemListener itemListener;
        public ViewHolder(@NonNull View itemView,ItemListener itemListener) {
            super(itemView);
            itemName=itemView.findViewById(R.id.itemOfCategoryName);
            itemPrice=itemView.findViewById(R.id.itemOfCategoryPrice);
            itemDate=itemView.findViewById(R.id.itemOfCategorydate);
            this.itemListener=itemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemListener.onClick(getAdapterPosition());
        }
    }
    public interface ItemListener{
        void onClick(int position);
    }
}
