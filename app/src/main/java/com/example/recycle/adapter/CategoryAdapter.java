package com.example.recycle.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recycle.R;
import com.example.recycle.model.category.CategoryModelForView;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    List<CategoryModelForView> categoryModelList;
    ItemListener itemListener;
    public CategoryAdapter(List<CategoryModelForView> categoryModelList,ItemListener itemListener) {
        this.categoryModelList = categoryModelList;
        this.itemListener=itemListener;
    }

    @NonNull
    @Override
    public CategoryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.category_shape,parent,false);
        ViewHolder viewHolder=new ViewHolder(v,itemListener);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryAdapter.ViewHolder holder, int position) {
        holder.categoryName.setText(categoryModelList.get(position).getCategoryModel().getCategoryName());
        holder.imageView.setImageBitmap(categoryModelList.get(position).getImageUrl());
    }

    @Override
    public int getItemCount() {
        return categoryModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView categoryName;
        ImageView imageView;
        ItemListener itemListener;
        public ViewHolder(@NonNull View itemView,ItemListener itemListener) {
            super(itemView);
            categoryName=itemView.findViewById(R.id.categoryText);
            imageView=itemView.findViewById(R.id.categoryIcon);
            this.itemListener=itemListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemListener.onClick(getAdapterPosition());
        }
    }
    public interface ItemListener{
        void onClick(int position);
    }
}
