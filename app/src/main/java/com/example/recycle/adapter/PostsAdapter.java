package com.example.recycle.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recycle.R;
import com.example.recycle.model.posts.PostsModel;

import java.util.List;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder>{

    List<PostsModel> postsModelList;
    public ItemListener onClickListner;

    public PostsAdapter(List<PostsModel> postsModelList, ItemListener onClickListner) {
        this.postsModelList = postsModelList;
        this.onClickListner = onClickListner;
    }

    @NonNull
    @Override
    public PostsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.posts_shap,parent,false);
        ViewHolder vh=new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull PostsAdapter.ViewHolder holder, int position) {
        holder.itemName.setText(postsModelList.get(position).getItemName());
        holder.date.setText(postsModelList.get(position).getDate());
        holder.price.setText(postsModelList.get(position).getPrice());
        holder.quantity.setText(String.valueOf(postsModelList.get(position).getQuantity()));
    }

    @Override
    public int getItemCount() {
        return postsModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView itemName;
        TextView date;
        TextView price;
        TextView quantity;
        ImageButton update;
        ImageButton delete;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            itemName=itemView.findViewById(R.id.postsItemName);
            date=itemView.findViewById(R.id.postsItemDate);
            price=itemView.findViewById(R.id.postsItemPrice);
            quantity=itemView.findViewById(R.id.postsQuantity);
            update=itemView.findViewById(R.id.updatePost);
            delete=itemView.findViewById(R.id.deletePost);
            update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListner.onUpdateClick(view,getAdapterPosition());
                }
            });
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onClickListner.onDeleteClick(view,getAdapterPosition());
                }
            });
        }

        @Override
        public void onClick(View view) {

        }
    }
    public interface ItemListener {
        public void onUpdateClick(View v,int position);
        public void onDeleteClick(View v,int position);
    }
}
