package com.example.recycle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.recycle.adapter.ImageAdapter;
import com.example.recycle.model.ResponseLoginModel;
import com.example.recycle.model.item_detail.ItemDetailModel;
import com.example.recycle.model.item_detail.ItemPicture;
import com.example.recycle.model.item_of_category.ItemOfCategoryModel;
import com.example.recycle.ui.item_of_category.ItemOfCategory;
import com.example.recycle.ui.search.SearchFragment;

import java.io.InputStream;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ItemDetail extends AppCompatActivity {
    ProgressBar progressBar;
    TextView categoryName;
    TextView itemName;
    TextView itemPrice;
    TextView owner;
    TextView date;
    TextView textQuantity;
    TextView textEmail;
    TextView availableQuantity;
    TextView description;
    TextView email;
    TextView quantityOrdered;
    TextView totalPrice;
    ImageButton add;
    ImageButton min;
    Button request;
    ViewPager mViewPager;
    int availableQuantities;
    int price=0;
    int ordered=0;
    int totalPriceVal=0;
    ItemOfCategoryModel itemOfCategoryModel;
    String BASE_URL="https://virutal.000webhostapp.com/RecycleAPI/ManageAccount/";
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    SharedPreferences sharedPreferences;
    ItemDetailModel itemDetailModel;
    private Bitmap imag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        initializeViews();
        Intent in=getIntent();
        HashMap<String,Integer> map=new HashMap<>();
        map.put("id_user",sharedPreferences.getInt("userId",-18));
        if(in.getIntExtra("type",2)==1)
            map.put("item_id", itemOfCategoryModel.getId());
        else
            map.put("item_id", SearchFragment.share.getId());
        Call<ItemDetailModel> call=retrofitInterface.executeGetItemDetail(map);
        call.enqueue(new Callback<ItemDetailModel>() {
            @Override
            public void onResponse(Call<ItemDetailModel> call, Response<ItemDetailModel> response) {
                if(response.code()==200&&response.body().getStatus()==1){
                    itemDetailModel=response.body();
                    for(ItemPicture i:itemDetailModel.getItemPictures()){
                        Call<ResponseBody> call2=retrofitInterface.executeGetImage(i.getUrl());
                        call2.enqueue(new Callback<ResponseBody>() {
                            @Override
                            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                if(response.isSuccessful()){
                                    InputStream is=response.body().byteStream();
                                    imag = BitmapFactory.decodeStream(is);
                                    setViewsVisible();
                                    setViewsData();
                                }else
                                    Toast.makeText(ItemDetail.this, response.message(), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(Call<ResponseBody> call, Throwable t) {
                                Toast.makeText(ItemDetail.this, t.toString(), Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                }

            }

            @Override
            public void onFailure(Call<ItemDetailModel> call, Throwable t) {
                Toast.makeText(ItemDetail.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ordered==Integer.parseInt(availableQuantity.getText().toString()))
                    return;

                ordered++;
                totalPriceVal+=price;
                totalPrice.setText(String.valueOf(totalPriceVal));
                quantityOrdered.setText(String.valueOf(ordered));
            }
        });
        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ordered==0)
                    return;
                ordered--;
                totalPriceVal-=price;
                totalPrice.setText(String.valueOf(totalPriceVal));
                quantityOrdered.setText(String.valueOf(ordered));
            }
        });

        request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ordered==0){
                    Toast.makeText(ItemDetail.this, "please add quantity", Toast.LENGTH_SHORT).show();
                }
                HashMap<String,Integer> map=new HashMap<>();
                map.put("id_user",sharedPreferences.getInt("userId",-18));
                map.put("item_id",itemOfCategoryModel.getId());
                map.put("quantity",ordered);
                Call<ResponseLoginModel> buy=retrofitInterface.executeBuyProducts(map);
                progressBar.setVisibility(View.VISIBLE);
                buy.enqueue(new Callback<ResponseLoginModel>() {
                    @Override
                    public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                        if (response.code()==200&&response.body().getStatus()==1){
                            Toast.makeText(ItemDetail.this, response.body().getAlert(), Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.INVISIBLE);
                        }
                        else
                            Toast.makeText(ItemDetail.this, response.body().getAlert(), Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                        Toast.makeText(ItemDetail.this, t.toString(), Toast.LENGTH_SHORT).show();
                    }
                });


            }
        });

    }
    void initializeViews(){
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        itemOfCategoryModel= ItemOfCategory.share;
        textQuantity=(TextView) findViewById(R.id.textQuantity);
        textEmail=(TextView) findViewById(R.id.textEmail);
        categoryName=(TextView) findViewById(R.id.itemDetailCategory);
        itemName=(TextView) findViewById(R.id.itemDetailName);
        itemPrice=(TextView) findViewById(R.id.itemDetailPrice);
        owner=(TextView) findViewById(R.id.itemDetailOwner);
        date=(TextView) findViewById(R.id.itemDetailDate);
        description=(TextView) findViewById(R.id.itemDetailDescription);
        email=(TextView) findViewById(R.id.itemDetailEmail);
        availableQuantity=(TextView) findViewById(R.id.itemDetailQuantityAvailable);
        quantityOrdered=(TextView) findViewById(R.id.itemDetailQuantityOrdered);
        totalPrice=(TextView) findViewById(R.id.itemDetailTotalPrice);
        add=(ImageButton) findViewById(R.id.itemDetailQuantityAdd);
        min=(ImageButton) findViewById(R.id.itemDetailQuantityMin);
        request=(Button) findViewById(R.id.requestItem);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        sharedPreferences=getSharedPreferences("myuser", Context.MODE_PRIVATE);
        progressBar=(ProgressBar) findViewById(R.id.progressBarForItemDetail);
    }

    void setViewsVisible(){
        textQuantity.setVisibility(View.VISIBLE);
        textEmail.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
        categoryName.setVisibility(View.VISIBLE);
        itemName.setVisibility(View.VISIBLE);
        itemPrice.setVisibility(View.VISIBLE);
        owner.setVisibility(View.VISIBLE);
        date.setVisibility(View.VISIBLE);
        description.setVisibility(View.VISIBLE);
        email.setVisibility(View.VISIBLE);
        availableQuantity.setVisibility(View.VISIBLE);
        quantityOrdered.setVisibility(View.VISIBLE);
        totalPrice.setVisibility(View.VISIBLE);
        add.setVisibility(View.VISIBLE);
        min.setVisibility(View.VISIBLE);
        request.setVisibility(View.VISIBLE);
    }

    void setViewsData(){
        ImageAdapter adapterView = new ImageAdapter(this,imag);
        mViewPager.setAdapter(adapterView);
        itemName.setText(itemDetailModel.getItemName());
        itemPrice.setText(itemDetailModel.getPrice());
        price=Integer.parseInt(itemDetailModel.getPrice());
        owner.setText(itemDetailModel.getOwnerName());
        date.setText(itemDetailModel.getDate());
        description.setText(itemDetailModel.getDescription());
        email.setText(itemDetailModel.getOwnerEmail());
        availableQuantities=itemDetailModel.getQuantity();
        availableQuantity.setText(String.valueOf(availableQuantities));
        totalPrice.setText(String.valueOf(itemDetailModel.getItemName()));
        quantityOrdered.setText(String.valueOf(ordered));
    }
}