package com.example.recycle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.recycle.model.ResponseLoginModel;
import com.google.android.gms.maps.model.LatLng;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SignUp extends AppCompatActivity {
    EditText fullName;
    EditText email;
    EditText password;
    TextView location;
    Button signup;
    LatLng latLng;
    RadioGroup accountType;
    RadioButton roleId;
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    String BASE_URL="https://virutal.000webhostapp.com/RecycleAPI/ManageAccount/";
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        fullName=(EditText) findViewById(R.id.signUpFullName);
        email=(EditText) findViewById(R.id.signUpEmail);
        password=(EditText) findViewById(R.id.signUpPassword);
        location=(TextView) findViewById(R.id.signUpLocation);
        signup=(Button) findViewById(R.id.signUp);
        accountType=(RadioGroup)findViewById(R.id.accountType);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        editor=getSharedPreferences("myuser", Context.MODE_PRIVATE).edit();
    }


    public boolean checkInputField() {
        if (fullName.getText().toString().equals("")) {
            Toast.makeText(SignUp.this, "Please enter your name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (email.getText().toString().equals("") || !email.getText().toString().contains("@")) {
            Toast.makeText(SignUp.this, "Please enter valid email ", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.getText().toString().equals("") || password.getText().toString().length() < 6) {
            Toast.makeText(SignUp.this, "Please Use 6 characters or more for your password ", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (location.getText().toString().contains("Enter")) {
            Toast.makeText(SignUp.this, "Please Enter your location", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void chooseLocation(View v){
        Intent in=new Intent(SignUp.this,MapsActivity.class);
        startActivityForResult(in,1);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1&&resultCode==RESULT_OK){
            Bundle bundle = data.getParcelableExtra("latlang");
            latLng=bundle.getParcelable("latlang");
            location.setText(String.valueOf(latLng.latitude)+" "+String.valueOf(latLng.longitude));
        }
    }
    public void onSignUpClicked(View v){
        if(checkInputField()){
            int role=accountType.getCheckedRadioButtonId();
            roleId=(RadioButton)findViewById(role);
            if(roleId.getText().toString().equals("Company"))
                role=2;
            else
                role=1;
            Call<ResponseLoginModel> call=retrofitInterface.executeSignup(fullName.getText().toString(),
                    email.getText().toString(),
                    password.getText().toString(),
                    role,
                    String.valueOf(latLng.latitude),
                    String.valueOf(latLng.longitude));
            call.enqueue(new Callback<ResponseLoginModel>() {
                @Override
                public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                    if(response.code()==200) {
                        ResponseLoginModel responseBody = response.body();
                        if(responseBody.getStatus()==1){
                            editor.putBoolean("Login",true).commit();
                            editor.putString("email",email.getText().toString()).commit();
                            editor.putString("password",password.getText().toString()).commit();
                            editor.putInt("userId",responseBody.getId()).commit();
                            editor.putInt("userRoleId",responseBody.getRoleId()).commit();
                            editor.putString("latitude",String.valueOf(responseBody.getLatitude())).commit();
                            editor.putString("longitude",String.valueOf(responseBody.getLongitude())).commit();
                            editor.putString("username",responseBody.getName()).commit();
                            Intent in=new Intent(SignUp.this,MainActivity.class);
                            startActivity(in);
                        }else{
                            Toast.makeText(SignUp.this, responseBody.getAlert(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                    Toast.makeText(SignUp.this, t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

}