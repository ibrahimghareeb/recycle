package com.example.recycle;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import com.example.recycle.model.ResponseLoginModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SplashScreen extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    String BASE_URL="https://virutal.000webhostapp.com/RecycleAPI/ManageAccount/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        sharedPreferences=getSharedPreferences("myuser", Context.MODE_PRIVATE);
        if(sharedPreferences.getBoolean("Login",false)){
            Call<ResponseLoginModel> call=retrofitInterface.executeLogin(sharedPreferences.getString("email",""),sharedPreferences.getString("password",""));
            call.enqueue(new Callback<ResponseLoginModel>() {
                @Override
                public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                    if(response.code()==200){
                        ResponseLoginModel responseLoginModel=response.body();
                        if(responseLoginModel.getStatus()==1){
                            Intent in=new Intent(SplashScreen.this,MainActivity.class);
                            startActivity(in);
                            finish();
                        }else{
                            Toast.makeText(SplashScreen.this,responseLoginModel.getAlert(),Toast.LENGTH_LONG).show();
                            Intent in=new Intent(SplashScreen.this,Login.class);
                            startActivity(in);
                            finish();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                    Toast.makeText(SplashScreen.this,t.toString(),Toast.LENGTH_LONG).show();
                    Intent in=new Intent(SplashScreen.this,Login.class);
                    startActivity(in);
                    finish();
                }
            });
        }else {
            Intent in = new Intent(SplashScreen.this, Login.class);
            startActivity(in);
            finish();
        }
    }
}