package com.example.recycle.model.posts;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PostsModelResponse {
    @SerializedName("items")
    List<PostsModel> posts;
    @SerializedName("status")
    int status;
    @SerializedName("alertMess")
    String alert;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public List<PostsModel> getPosts() {
        return posts;
    }

    public void setPosts(List<PostsModel> posts) {
        this.posts = posts;
    }

    public PostsModelResponse(List<PostsModel> posts, int status, String alert) {
        this.posts = posts;
        this.status = status;
        this.alert = alert;
    }
}
