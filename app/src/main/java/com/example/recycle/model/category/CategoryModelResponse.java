package com.example.recycle.model.category;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryModelResponse {
    @SerializedName("categories")
    List<CategoryModel> categoryModelList;
    @SerializedName("status")
    int status;
    @SerializedName("alertMess")
    String alert;

    public CategoryModelResponse(List<CategoryModel> categoryModelList, int status, String alert) {
        this.categoryModelList = categoryModelList;
        this.status = status;
        this.alert = alert;
    }

    public List<CategoryModel> getCategoryModelList() {
        return categoryModelList;
    }

    public void setCategoryModelList(List<CategoryModel> categoryModelList) {
        this.categoryModelList = categoryModelList;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }
}
