package com.example.recycle.model.category;

import android.graphics.Bitmap;

public class CategoryModelForView {
    CategoryModel categoryModel;
    Bitmap imageUrl;

    public CategoryModelForView(CategoryModel categoryModel, Bitmap imageUrl) {
        this.categoryModel = categoryModel;
        this.imageUrl = imageUrl;
    }

    public CategoryModel getCategoryModel() {
        return categoryModel;
    }

    public void setCategoryModel(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    public Bitmap getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Bitmap imageUrl) {
        this.imageUrl = imageUrl;
    }
}
