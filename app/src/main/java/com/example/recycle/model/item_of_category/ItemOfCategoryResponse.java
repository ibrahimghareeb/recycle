package com.example.recycle.model.item_of_category;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemOfCategoryResponse {
    @SerializedName("items")
    List<ItemOfCategoryModel> itemOfCategories;
    @SerializedName("status")
    int status;
    @SerializedName("alertMess")
    String alert;

    public ItemOfCategoryResponse(List<ItemOfCategoryModel> itemOfCategories, int status, String alert) {
        this.itemOfCategories = itemOfCategories;
        this.status = status;
        this.alert = alert;
    }

    public List<ItemOfCategoryModel> getItemOfCategories() {
        return itemOfCategories;
    }

    public void setItemOfCategories(List<ItemOfCategoryModel> itemOfCategories) {
        this.itemOfCategories = itemOfCategories;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }
}
