package com.example.recycle.model.my_orders;

import com.google.gson.annotations.SerializedName;

public class MyOrdersModel {
    @SerializedName("id_request")
    int id;
    @SerializedName("item_id")
    int itemId;
    @SerializedName("item_name")
    String itemName;
    @SerializedName("added_date")
    String date;
    @SerializedName("item_price")
    String price;

    public MyOrdersModel(int id, int itemId, String itemName, String date, String price) {
        this.id = id;
        this.itemId = itemId;
        this.itemName = itemName;
        this.date = date;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
