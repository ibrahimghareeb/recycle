package com.example.recycle.model.item_detail;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemDetailModel {
    @SerializedName("item_id")
    int id;
    @SerializedName("item_name")
    String itemName;
    @SerializedName("added_date")
    String date;
    @SerializedName("item_price")
    String price;
    @SerializedName("item_quantity")
    int quantity;
    @SerializedName("item_details")
    String description;
    @SerializedName("Email_user")
    String ownerEmail;
    @SerializedName("Name_user")
    String ownerName;
    @SerializedName("id_category")
    int idCategory;
    @SerializedName("itempictures")
    List<ItemPicture> itemPictures;
    @SerializedName("status")
    int status;
    @SerializedName("alertMess")
    String alert;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public ItemDetailModel(int id, String itemName, String date, String price, int quantity, String description, String ownerEmail, String ownerName, int idCategory, List<ItemPicture> itemPictures, int status, String alert) {
        this.id = id;
        this.itemName = itemName;
        this.date = date;
        this.price = price;
        this.quantity = quantity;
        this.description = description;
        this.ownerEmail = ownerEmail;
        this.ownerName = ownerName;
        this.idCategory = idCategory;
        this.itemPictures = itemPictures;
        this.status = status;
        this.alert = alert;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOwnerEmail() {
        return ownerEmail;
    }

    public void setOwnerEmail(String ownerEmail) {
        this.ownerEmail = ownerEmail;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public List<ItemPicture> getItemPictures() {
        return itemPictures;
    }

    public void setItemPictures(List<ItemPicture> itemPictures) {
        this.itemPictures = itemPictures;
    }
}
