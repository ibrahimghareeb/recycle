package com.example.recycle.model.category;

import com.google.gson.annotations.SerializedName;

public class CategoryModel {
    @SerializedName("id_category")
    int id;
    @SerializedName("name_category")
    String categoryName;
    @SerializedName("cat_Icon")
    String iconUrl;

    public CategoryModel(int id, String categoryName, String iconUrl) {
        this.id = id;
        this.categoryName = categoryName;
        this.iconUrl = iconUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
