package com.example.recycle.model.item_detail;

import com.google.gson.annotations.SerializedName;

import java.io.File;

public class ItemPicture {
    @SerializedName("picture_url")
    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ItemPicture(String url) {
        this.url = url;
    }
}
