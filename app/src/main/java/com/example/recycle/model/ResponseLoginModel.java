package com.example.recycle.model;

import com.google.gson.annotations.SerializedName;

public class ResponseLoginModel {
    @SerializedName("id_user")
    int id;
    @SerializedName("Name_user")
    String name;
    @SerializedName("Email_user")
    String email;
    @SerializedName("Password_user")
    String password;
    @SerializedName("UserRoleId")
    int roleId;
    @SerializedName("L_latitude")
    double latitude;
    @SerializedName("L_longitude")
    double longitude;
    @SerializedName("status")
    int status;
    @SerializedName("alertMess")
    String alert;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public int getRoleId() {
        return roleId;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getStatus() {
        return status;
    }

    public String getAlert() {
        return alert;
    }
}
