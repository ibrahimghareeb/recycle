package com.example.recycle.model.posts;

import com.google.gson.annotations.SerializedName;

public class PostsModel {
    @SerializedName("item_id")
    int id;
    @SerializedName("item_name")
    String itemName;
    @SerializedName("added_date")
    String date;
    @SerializedName("item_price")
    String price;
    @SerializedName("quantity")
    int quantity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public PostsModel(int id, String itemName, String date, String price, int quantity) {
        this.id = id;
        this.itemName = itemName;
        this.date = date;
        this.price = price;
        this.quantity = quantity;
    }
}
