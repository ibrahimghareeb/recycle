package com.example.recycle.model.search_model;

import com.example.recycle.model.posts.PostsModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SearchModelResponse {
    public List<SearchModel> getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(List<SearchModel> searchResult) {
        this.searchResult = searchResult;
    }

    public SearchModelResponse(List<SearchModel> searchResult, int status, String alert) {
        this.searchResult = searchResult;
        this.status = status;
        this.alert = alert;
    }

    @SerializedName("items")
    List<SearchModel> searchResult;
    @SerializedName("status")
    int status;
    @SerializedName("alertMess")
    String alert;



    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }
}
