package com.example.recycle.model.request;

import com.google.gson.annotations.SerializedName;

public class RequestModel {
    @SerializedName("item_id")
    int id;
    @SerializedName("item_name")
    String itemName;
    @SerializedName("Name_company")
    String nameCompany;
    @SerializedName("added_date")
    String date;
    @SerializedName("item_price")
    String price;
    @SerializedName("quantity")
    int quantity;

    public RequestModel(int id, String itemName, String nameCompany, String date, String price, int quantity) {
        this.id = id;
        this.itemName = itemName;
        this.nameCompany = nameCompany;
        this.date = date;
        this.price = price;
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getNameCompany() {
        return nameCompany;
    }

    public void setNameCompany(String nameCompany) {
        this.nameCompany = nameCompany;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
