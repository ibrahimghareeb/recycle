package com.example.recycle.model.my_orders;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyOrdersModelResponse {
    @SerializedName("items")
    List<MyOrdersModel> myOrdersModels;
    @SerializedName("status")
    int status;
    @SerializedName("alertMess")
    String alert;

    public MyOrdersModelResponse(List<MyOrdersModel> myOrdersModels, int status, String alert) {
        this.myOrdersModels = myOrdersModels;
        this.status = status;
        this.alert = alert;
    }

    public List<MyOrdersModel> getMyOrdersModels() {
        return myOrdersModels;
    }

    public void setMyOrdersModels(List<MyOrdersModel> myOrdersModels) {
        this.myOrdersModels = myOrdersModels;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }
}
