package com.example.recycle.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RequestsModelResponse {
    @SerializedName("items")
    List<RequestModel> requestModelList;
    @SerializedName("status")
    int status;
    @SerializedName("alertMess")
    String alert;

    public List<RequestModel> getRequestModelList() {
        return requestModelList;
    }

    public void setRequestModelList(List<RequestModel> requestModelList) {
        this.requestModelList = requestModelList;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getAlert() {
        return alert;
    }

    public void setAlert(String alert) {
        this.alert = alert;
    }

    public RequestsModelResponse(List<RequestModel> requestModelList, int status, String alert) {
        this.requestModelList = requestModelList;
        this.status = status;
        this.alert = alert;
    }
}
