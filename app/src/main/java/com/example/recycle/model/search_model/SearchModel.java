package com.example.recycle.model.search_model;

import com.google.gson.annotations.SerializedName;

public class SearchModel {
    @SerializedName("item_id")
    int id;
    @SerializedName("item_name")
    String itemName;
    @SerializedName("added_date")
    String date;
    @SerializedName("item_price")
    String price;

    public SearchModel(int id, String itemName, String date, String price) {
        this.id = id;
        this.itemName = itemName;
        this.date = date;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
