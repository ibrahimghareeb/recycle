package com.example.recycle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.recycle.model.ResponseLoginModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Login extends AppCompatActivity {
    Button login;
    EditText email;
    EditText password;
    TextView signUp;
    String BASE_URL="https://virutal.000webhostapp.com/RecycleAPI/ManageAccount/";
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
        }
        setContentView(R.layout.activity_login);
        login=(Button) findViewById(R.id.login);
        email=(EditText) findViewById(R.id.loginEmail);
        password=(EditText) findViewById(R.id.loginPassword);
        signUp=(TextView) findViewById(R.id.signupTextView);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        editor=getSharedPreferences("myuser", Context.MODE_PRIVATE).edit();
    }
    public void onSignUpClicked(View v){
        Intent in=new Intent(Login.this,SignUp.class);
        startActivity(in);
    }
    public void onLoginClicked(View v){
        if(checkInputField()){
            Call<ResponseLoginModel> call=retrofitInterface.executeLogin(email.getText().toString(),password.getText().toString());
            call.enqueue(new Callback<ResponseLoginModel>() {
                @Override
                public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                    if(response.code()==200) {
                        ResponseLoginModel responseBody = response.body();
                        if(responseBody.getStatus()==1){
                            editor.putBoolean("Login",true).commit();
                            editor.putString("email",email.getText().toString()).commit();
                            editor.putString("password",password.getText().toString()).commit();
                            editor.putInt("userId",responseBody.getId()).commit();
                            editor.putInt("userRoleId",responseBody.getRoleId()).commit();
                            editor.putString("latitude",String.valueOf(responseBody.getLatitude())).commit();
                            editor.putString("longitude",String.valueOf(responseBody.getLongitude())).commit();
                            editor.putString("username",responseBody.getName()).commit();
                            Intent in=new Intent(Login.this,MainActivity.class);
                            startActivity(in);
                        }else{
                            Toast.makeText(Login.this, responseBody.getAlert(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                    Toast.makeText(Login.this, t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public boolean checkInputField(){
        if (email.getText().toString().equals("") || !email.getText().toString().contains("@")){
            Toast.makeText(Login.this,"Please enter valid email ",Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.getText().toString().equals("") || password.getText().toString().length()<6){
            Toast.makeText(Login.this,"Please Use 6 characters or more for your password ",Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Please grant permission befor start app", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}