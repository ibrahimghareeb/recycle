package com.example.recycle.ui.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recycle.MainActivity;
import com.example.recycle.R;
import com.example.recycle.RetrofitInterface;
import com.example.recycle.Space;
import com.example.recycle.adapter.CategoryAdapter;
import com.example.recycle.model.category.CategoryModel;
import com.example.recycle.model.category.CategoryModelForView;
import com.example.recycle.model.category.CategoryModelResponse;
import com.example.recycle.ui.add_item.AddItemFragment;
import com.example.recycle.ui.item_of_category.ItemOfCategory;
import com.example.recycle.ui.search.SearchFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class HomeFragment extends Fragment implements CategoryAdapter.ItemListener {
    RecyclerView recyclerViewForCategory;
    String BASE_URL="https://virutal.000webhostapp.com/RecycleAPI/ManageAccount/";
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    SharedPreferences sharedPreferences;
    ProgressBar progressBar;
    FloatingActionButton fab;
    public static CategoryModelResponse categoryModelResponse;
    List<CategoryModelForView> categoryModelForViewList;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        setHasOptionsMenu(true);
        recyclerViewForCategory=(RecyclerView) root.findViewById(R.id.recyclerViewForCategory);
        fab=(FloatingActionButton) root.findViewById(R.id.fab);
        progressBar=(ProgressBar)root.findViewById(R.id.progressBarHome);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        categoryModelForViewList=new ArrayList<>();
        sharedPreferences=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddItemFragment addItemFragment=new AddItemFragment(categoryModelResponse);
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,addItemFragment).addToBackStack(null).commit();
            }
        });
        Call<CategoryModelResponse> call=retrofitInterface.executeGetCategory(sharedPreferences.getInt("userId",-18));
        call.enqueue(new Callback<CategoryModelResponse>() {
            @Override
            public void onResponse(Call<CategoryModelResponse> call, Response<CategoryModelResponse> response) {
                if(response.code()==200){
                    categoryModelResponse=response.body();
                    if(categoryModelResponse.getStatus()==1){
                        for(CategoryModel i:categoryModelResponse.getCategoryModelList()){
                            if(i.getIconUrl().toString().contains("localhost")){
                                Toast.makeText(getActivity(), "skip", Toast.LENGTH_SHORT).show();
                                continue;
                            }

                            Call<ResponseBody> call2=retrofitInterface.executeGetImage(i.getIconUrl());
                            call2.enqueue(new Callback<ResponseBody>() {
                                @Override
                                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                                    if(response.isSuccessful()){
                                        InputStream is=response.body().byteStream();
                                        Bitmap imag = BitmapFactory.decodeStream(is);
                                        categoryModelForViewList.add(new CategoryModelForView(i,Bitmap.createScaledBitmap(imag,150,150,false)));
                                        progressBar.setVisibility(View.INVISIBLE);
                                        fab.setVisibility(View.VISIBLE);
                                        printRecyclerView();
                                    }
                                    else
                                        Toast.makeText(getActivity(), response.body().toString(), Toast.LENGTH_LONG).show();
                                }

                                @Override
                                public void onFailure(Call<ResponseBody> call, Throwable t) {
                                    Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    }
                }
            }
            @Override
            public void onFailure(Call<CategoryModelResponse> call, Throwable t) {

            }
        });

        return root;
    }

    private void printRecyclerView() {
        CategoryAdapter categoryAdapter=new CategoryAdapter(categoryModelForViewList,this);
        recyclerViewForCategory.setLayoutManager(new GridLayoutManager(getContext(),2));
        recyclerViewForCategory.setItemAnimator(new DefaultItemAnimator());
        recyclerViewForCategory.addItemDecoration(new Space(10));
        recyclerViewForCategory.setAdapter(categoryAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onClick(int position) {
        ItemOfCategory itemOfCategory=new ItemOfCategory(categoryModelForViewList.get(position).getCategoryModel());
        getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,itemOfCategory).addToBackStack(null).commit();
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        MainActivity.actionBar.setTitle("Home");
//    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
        switch (item.getItemId()){
            case R.id.action_search:
                MainActivity.actionBar.setTitle("search");
                SearchFragment searchFragment=new SearchFragment(categoryModelResponse);
                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,searchFragment).addToBackStack(null).commit();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        MainActivity.actionBar.setTitle("Home");
    }
}