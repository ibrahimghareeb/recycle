package com.example.recycle.ui.my_request;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.recycle.MainActivity;
import com.example.recycle.R;
import com.example.recycle.RetrofitInterface;
import com.example.recycle.Space;
import com.example.recycle.adapter.OrdersAdapter;
import com.example.recycle.adapter.PostsAdapter;
import com.example.recycle.adapter.RequestAdapter;
import com.example.recycle.model.ResponseLoginModel;
import com.example.recycle.model.my_orders.MyOrdersModel;
import com.example.recycle.model.my_orders.MyOrdersModelResponse;
import com.example.recycle.model.request.RequestModel;
import com.example.recycle.model.request.RequestsModelResponse;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class MyRequestsFragment extends Fragment implements RequestAdapter.ItemListener, OrdersAdapter.ItemListener {

    RecyclerView recyclerViewForMyRequests;
    String BASE_URL="https://virutal.000webhostapp.com/RecycleAPI/ManageAccount/";
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    SharedPreferences sharedPreferences;
    ProgressBar progressBar;
    List<RequestModel> requestsModelResponseList;
    List<MyOrdersModel> myOrdersModels;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root=inflater.inflate(R.layout.fragment_my_request, container, false);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        recyclerViewForMyRequests=(RecyclerView)root.findViewById(R.id.recyclerViewForMyRequests);
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        sharedPreferences=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        progressBar=(ProgressBar) root.findViewById(R.id.progressBarMyRequests);
        HashMap<String,Integer> map= new HashMap<String, Integer>();
        map.put("id_user",sharedPreferences.getInt("userId",-18));
        if(sharedPreferences.getInt("userRoleId",-18)==1) {
            Call<RequestsModelResponse> call = retrofitInterface.executeGetMyRequests(map);
            call.enqueue(new Callback<RequestsModelResponse>() {
                @Override
                public void onResponse(Call<RequestsModelResponse> call, Response<RequestsModelResponse> response) {
                    if (response.code() == 200 && response.body().getStatus() == 1) {
                        requestsModelResponseList = response.body().getRequestModelList();
                        initializeRecyclerView();
                        progressBar.setVisibility(View.INVISIBLE);
                    } else {
                        Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<RequestsModelResponse> call, Throwable t) {
                    Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.INVISIBLE);

                }
            });
        }else if(sharedPreferences.getInt("userRoleId",-18)==2){
            MainActivity.actionBar.setTitle("My Orders");
            Call<MyOrdersModelResponse> call = retrofitInterface.executeGetMyOrders(map);
            call.enqueue(new Callback<MyOrdersModelResponse>() {
                @Override
                public void onResponse(Call<MyOrdersModelResponse> call, Response<MyOrdersModelResponse> response) {
                    if (response.code() == 200 && response.body().getStatus() == 1) {
                        myOrdersModels = response.body().getMyOrdersModels();
                        initializeRecyclerView();
                        progressBar.setVisibility(View.INVISIBLE);
                    } else {
                        Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.INVISIBLE);
                    }
                }

                @Override
                public void onFailure(Call<MyOrdersModelResponse> call, Throwable t) {
                    Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.INVISIBLE);

                }
            });
        }
        return root;
    }
    void initializeRecyclerView(){
        recyclerViewForMyRequests.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewForMyRequests.setItemAnimator(new DefaultItemAnimator());
        recyclerViewForMyRequests.addItemDecoration(new Space(10));
        if(sharedPreferences.getInt("userRoleId",-18)==1){
            RequestAdapter requestAdapter=new RequestAdapter(requestsModelResponseList,this);
            recyclerViewForMyRequests.setAdapter(requestAdapter);
        }else if (sharedPreferences.getInt("userRoleId",-18)==2){
            OrdersAdapter ordersAdapter=new OrdersAdapter(myOrdersModels,this);
            recyclerViewForMyRequests.setAdapter(ordersAdapter);
        }

    }


    @Override
    public void onAcceptClick(View v, int position) {
        Toast.makeText(getActivity(), "clicked", Toast.LENGTH_SHORT).show();
        HashMap<String,Object> map=new HashMap<>();
        map.put("id_user",sharedPreferences.getInt("userId",-18));
        map.put("item_id",requestsModelResponseList.get(position).getId());
        map.put("status","accept");
        Call<ResponseLoginModel> call=retrofitInterface.executeAcceptOrRefuse(map);
        call.enqueue(new Callback<ResponseLoginModel>() {
            @Override
            public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                if(response.code()==200&&response.body().getStatus()==1){
                    Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
                    requestsModelResponseList.remove(position);
                    initializeRecyclerView();
                }else
                    Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onRefuseClick(View v, int position) {
        HashMap<String,Object> map=new HashMap<>();
        map.put("id_user",sharedPreferences.getInt("userId",-18));
        map.put("item_id",requestsModelResponseList.get(position).getId());
        map.put("status","refuse");
        Call<ResponseLoginModel> call=retrofitInterface.executeAcceptOrRefuse(map);
        call.enqueue(new Callback<ResponseLoginModel>() {
            @Override
            public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                if(response.code()==200&&response.body().getStatus()==1){
                    Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
                    requestsModelResponseList.remove(position);
                    initializeRecyclerView();
                }else
                    Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDeleteClick(View v, int position) {
        HashMap <String,Integer>map=new HashMap<>();
        map.put("id_user",sharedPreferences.getInt("userId",-18));
        map.put("id_request",myOrdersModels.get(position).getId());
        Call<ResponseLoginModel> call=retrofitInterface.executeDeleteOrder(map);
        call.enqueue(new Callback<ResponseLoginModel>() {
            @Override
            public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                if(response.code()==200 && response.body().getStatus()==1){
                    myOrdersModels.remove(position);
                    initializeRecyclerView();
                    Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
                }else
                    Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}