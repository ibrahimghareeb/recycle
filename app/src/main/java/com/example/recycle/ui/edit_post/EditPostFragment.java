package com.example.recycle.ui.edit_post;

import static android.app.Activity.RESULT_OK;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.recycle.R;
import com.example.recycle.RetrofitInterface;
import com.example.recycle.model.ResponseLoginModel;
import com.example.recycle.model.category.CategoryModel;
import com.example.recycle.model.category.CategoryModelResponse;
import com.example.recycle.ui.home.HomeFragment;
import com.example.recycle.ui.my_posts.MyPostsFragment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class EditPostFragment extends Fragment {

    private static final int IMAGE_REQUEST_ID = 1;
    EditText itemName;
    Spinner category;
    EditText description;
    int postId;
    int quantityValue=0;
    ImageButton add;
    ImageButton min;
    TextView quantity;
    EditText price;
    ImageButton addPhoto;
    Button post;
    Bitmap img;
    Uri temp;
    String BASE_URL="https://virutal.000webhostapp.com/RecycleAPI/ManageAccount/";
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    SharedPreferences sharedPreferences;
    CategoryModelResponse categoryModelResponse;

    public EditPostFragment(int postId) {
        this.postId = postId;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_edit_post, container, false);
        itemName=view.findViewById(R.id.editItemName);
        category=view.findViewById(R.id.editItemCategory);
        description=view.findViewById(R.id.editItemDescription);
        add=view.findViewById(R.id.editQuantityAdd);
        min=view.findViewById(R.id.editQuantityMin);
        quantity=view.findViewById(R.id.editQuantityNumber);
        price=view.findViewById(R.id.editItemPrice);
        post=view.findViewById(R.id.editPostItem);
        addPhoto=view.findViewById(R.id.editPostaddPhoto);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        sharedPreferences=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        categoryModelResponse= HomeFragment.categoryModelResponse;
        initializeSpinner();
        addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(in, "choose Image"), IMAGE_REQUEST_ID);
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quantityValue++;
                quantity.setText(String.valueOf(quantityValue));
            }
        });
        min.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(quantityValue>0)
                    quantityValue--;
                quantity.setText(String.valueOf(quantityValue));
            }
        });
        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkInputField()){
                    File file=new File(getRealPathFromURI(temp));
                    RequestBody image = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    Call<ResponseLoginModel> call=retrofitInterface.executeEditPost(sharedPreferences.getInt("userId",-18),
                            postId,
                            itemName.getText().toString(),
                            quantityValue,
                            description.getText().toString(),
                            getCategoryId(category.getSelectedItem().toString()),
                            Integer.parseInt(price.getText().toString()),
                            MultipartBody.Part.createFormData("itempictures[]",file.getName(),image));
                    call.enqueue(new Callback<ResponseLoginModel>() {
                        @Override
                        public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                            if(response.code()==200 && response.body().getStatus()==1){
                                Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
                                MyPostsFragment myPostsFragment=new MyPostsFragment();
                                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,myPostsFragment).commit();
                            }

                            else
                                Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<ResponseLoginModel> call, Throwable t) {

                        }
                    });
                }
            }
        });
        return view;
    }
    void initializeSpinner (){
        ArrayList<String> arrayList=new ArrayList<>();
        for (CategoryModel i:categoryModelResponse.getCategoryModelList()){
            arrayList.add(i.getCategoryName());
        }
        ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(arrayAdapter);
    }

    private int getCategoryId(String selectedItem) {
        for (CategoryModel i:categoryModelResponse.getCategoryModelList()){
            if(i.getCategoryName().equals(selectedItem))
                return i.getId();
        }
        return 0;
    }

    public boolean checkInputField (){
        if(itemName.getText().toString().equals("")){
            Toast.makeText(getActivity(),"Enter item name",Toast.LENGTH_LONG).show();
            return false;
        }
        if(description.getText().toString().equals("")){
            Toast.makeText(getActivity(),"Enter item description",Toast.LENGTH_LONG).show();
            return false;
        }
        if(quantityValue==0){
            Toast.makeText(getActivity(),"Enter quantity number",Toast.LENGTH_LONG).show();
            return false;
        }
        if(img==null){
            Toast.makeText(getActivity(),"Enter photo",Toast.LENGTH_LONG).show();
            return false;
        }
        if(price.getText().toString().equals("")){
            Toast.makeText(getActivity(),"Enter price",Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_REQUEST_ID && resultCode == RESULT_OK
                && data != null) {
            try {
                img = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                temp=data.getData();
            } catch (IOException e) {
                Toast.makeText(getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
            }
        }
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
}