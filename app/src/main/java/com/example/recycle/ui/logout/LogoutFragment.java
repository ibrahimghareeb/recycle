package com.example.recycle.ui.logout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.recycle.Login;
import com.example.recycle.R;

public class LogoutFragment extends Fragment {
    SharedPreferences.Editor editor;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        editor=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE).edit();
        editor.remove("email");
        editor.remove("password");
        editor.putBoolean("Login", false);
        editor.apply();
        Intent in=new Intent(getActivity(), Login.class);
        startActivity(in);
        getActivity().finish();
    }
}