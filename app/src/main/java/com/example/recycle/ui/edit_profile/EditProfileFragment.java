package com.example.recycle.ui.edit_profile;

import static android.app.Activity.RESULT_OK;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.recycle.MainActivity;
import com.example.recycle.MapsActivity;
import com.example.recycle.R;
import com.example.recycle.RetrofitInterface;
import com.example.recycle.SignUp;
import com.example.recycle.model.ResponseLoginModel;
import com.example.recycle.ui.home.HomeFragment;
import com.example.recycle.ui.search.SearchFragment;
import com.google.android.gms.maps.model.LatLng;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class EditProfileFragment extends Fragment {
    EditText fullName;
    EditText email;
    EditText password;
    TextView location;
    Button confirm;
    LatLng latLng;
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    String BASE_URL="https://virutal.000webhostapp.com/RecycleAPI/ManageAccount/";
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root =inflater.inflate(R.layout.fragment_edit_profile, container, false);
        fullName=(EditText) root.findViewById(R.id.editProfileFullName);
        email=(EditText) root.findViewById(R.id.editProfileEmail);
        password=(EditText) root.findViewById(R.id.editProfilePassword);
        location=(TextView) root.findViewById(R.id.editProfileLocation);
        confirm=(Button) root.findViewById(R.id.confirm);
        sharedPreferences=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        editor=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE).edit();

        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in=new Intent(getActivity(), MapsActivity.class);
                startActivityForResult(in,1);
            }
        });
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkInputField()){
                    Call<ResponseLoginModel> call=retrofitInterface.executeEditProfile(sharedPreferences.getInt("userId",-18),
                            email.getText().toString(),
                            password.getText().toString(),
                            fullName.getText().toString(),
                            String.valueOf(latLng.latitude),
                            String.valueOf(latLng.longitude));
                    call.enqueue(new Callback<ResponseLoginModel>() {
                        @Override
                        public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                            if(response.code()==200&&response.body().getStatus()==1){
                                Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
                                editor.putString("email",email.getText().toString()).commit();
                                editor.putString("password",password.getText().toString()).commit();
                                editor.putString("latitude",String.valueOf(response.body().getLatitude())).commit();
                                editor.putString("longitude",String.valueOf(response.body().getLongitude())).commit();
                                editor.putString("username",response.body().getName()).commit();
                                HomeFragment homeFragment=new HomeFragment();
                                getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,homeFragment).commit();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseLoginModel> call, Throwable t) {

                        }
                    });
                }
            }
        });
        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==1&&resultCode==RESULT_OK){
            Bundle bundle = data.getParcelableExtra("latlang");
            latLng=bundle.getParcelable("latlang");
            location.setText(String.valueOf(latLng.latitude)+" "+String.valueOf(latLng.longitude));
        }
    }
    public boolean checkInputField() {
        if (fullName.getText().toString().equals("")) {
            Toast.makeText(getActivity(), "Please enter your name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (email.getText().toString().equals("") || !email.getText().toString().contains("@")) {
            Toast.makeText(getActivity(), "Please enter valid email ", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (password.getText().toString().equals("") || password.getText().toString().length() < 6) {
            Toast.makeText(getActivity(), "Please Use 6 characters or more for your password ", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (location.getText().toString().contains("Enter")) {
            Toast.makeText(getActivity(), "Please Enter your location", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

}