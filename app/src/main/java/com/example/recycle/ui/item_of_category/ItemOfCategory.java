package com.example.recycle.ui.item_of_category;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.collection.ArrayMap;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.recycle.ItemDetail;
import com.example.recycle.MainActivity;
import com.example.recycle.R;
import com.example.recycle.RetrofitInterface;
import com.example.recycle.Space;
import com.example.recycle.adapter.CategoryAdapter;
import com.example.recycle.adapter.ItemOfCategoryAdapter;
import com.example.recycle.model.category.CategoryModel;
import com.example.recycle.model.item_of_category.ItemOfCategoryModel;
import com.example.recycle.model.item_of_category.ItemOfCategoryResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class ItemOfCategory extends Fragment implements ItemOfCategoryAdapter.ItemListener {
    RecyclerView recyclerView;
    CategoryModel categoryModel;
    String BASE_URL="https://virutal.000webhostapp.com/RecycleAPI/ManageAccount/";
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    SharedPreferences sharedPreferences;
    ItemOfCategoryResponse itemOfCategoryResponse;
    public static ItemOfCategoryModel share;
    ProgressBar progressBar;
    public ItemOfCategory(CategoryModel categoryModel) {
        this.categoryModel = categoryModel;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity.actionBar.setTitle(categoryModel.getCategoryName());
        View view=inflater.inflate(R.layout.fragment_item_of_category, container, false);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        recyclerView=(RecyclerView)view.findViewById(R.id.recyclerViewForItemOfCategory);
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        sharedPreferences=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        progressBar=(ProgressBar) view.findViewById(R.id.progressBarItemOfCategory);
        HashMap<String,Integer> map= new HashMap<String, Integer>();
        map.put("id_user",sharedPreferences.getInt("userId",-18));
        map.put("id_category",categoryModel.getId());
        Call<ItemOfCategoryResponse> call=retrofitInterface.executeGetItemOfCategory(map);
        call.enqueue(new Callback<ItemOfCategoryResponse>() {
            @Override
            public void onResponse(Call<ItemOfCategoryResponse> call, Response<ItemOfCategoryResponse> response) {
                if(response.code()==200&&response.body().getStatus()==1){
                    itemOfCategoryResponse =response.body();
                    printRecyclerView();
                    progressBar.setVisibility(View.INVISIBLE);
                }
                else {
                    Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.INVISIBLE);
                    
                }
            }

            @Override
            public void onFailure(Call<ItemOfCategoryResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });


        return view;
    }
    private void printRecyclerView() {
        ItemOfCategoryAdapter categoryAdapter=new ItemOfCategoryAdapter(itemOfCategoryResponse.getItemOfCategories(),this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new Space(10));
        recyclerView.setAdapter(categoryAdapter);
    }

    @Override
    public void onClick(int position) {
        share=itemOfCategoryResponse.getItemOfCategories().get(position);
        Intent in=new Intent(getActivity(), ItemDetail.class);
        in.putExtra("type",1);
        startActivity(in);
    }
}