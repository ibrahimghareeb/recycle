package com.example.recycle.ui.search;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.recycle.ItemDetail;
import com.example.recycle.Login;
import com.example.recycle.R;
import com.example.recycle.RetrofitInterface;
import com.example.recycle.Space;
import com.example.recycle.adapter.PostsAdapter;
import com.example.recycle.adapter.SearchAdapter;
import com.example.recycle.model.category.CategoryModel;
import com.example.recycle.model.category.CategoryModelResponse;
import com.example.recycle.model.search_model.SearchModel;
import com.example.recycle.model.search_model.SearchModelResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class SearchFragment extends Fragment implements SearchAdapter.ItemListener {

    EditText itemName;
    Spinner category;
    RecyclerView recyclerViewForSearch;
    Button search;
    String BASE_URL="https://virutal.000webhostapp.com/RecycleAPI/ManageAccount/";
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    SharedPreferences sharedPreferences;
    CategoryModelResponse categoryModelResponse;
    List<SearchModel> searchModels;
    public static SearchModel share;
    public SearchFragment(CategoryModelResponse categoryModelResponse) {
        this.categoryModelResponse = categoryModelResponse;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View root=inflater.inflate(R.layout.fragment_search, container, false);
        itemName=(EditText) root.findViewById(R.id.searchItemName);
        category=(Spinner) root.findViewById(R.id.searchItemCategory);
        search=(Button) root.findViewById(R.id.searchItem);
        retrofit =new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        recyclerViewForSearch=(RecyclerView)root.findViewById(R.id.recyclerViewForSearch);
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        sharedPreferences=getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        initializeSpinner();
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(checkInputField()){
                    HashMap<String,Object> map=new HashMap<>();
                    map.put("id_user",sharedPreferences.getInt("userId",-18));
                    map.put("id_category",getCategoryId(category.getSelectedItem().toString()));
                    map.put("item_name",itemName.getText().toString());
                    Call<SearchModelResponse> call=retrofitInterface.executeSearch(map);
                    call.enqueue(new Callback<SearchModelResponse>() {
                        @Override
                        public void onResponse(Call<SearchModelResponse> call, Response<SearchModelResponse> response) {
                            if(response.code()==200&&response.body().getStatus()==1){
                                searchModels=response.body().getSearchResult();
                                initializeRecyclerView();
                            }else
                                Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<SearchModelResponse> call, Throwable t) {
                            Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });
        return root;
    }
    void initializeSpinner (){
        ArrayList<String> arrayList=new ArrayList<>();
        for (CategoryModel i:categoryModelResponse.getCategoryModelList()){
            arrayList.add(i.getCategoryName());
        }
        ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category.setAdapter(arrayAdapter);
    }
    public boolean checkInputField() {
        if (itemName.getText().toString().equals("") ) {
            Toast.makeText(getActivity(), "Please enter item name ", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
   int getCategoryId(String selectedItem) {
        for (CategoryModel i:categoryModelResponse.getCategoryModelList()){
            if(i.getCategoryName().equals(selectedItem))
                return i.getId();
        }
        return 0;
    }
    void initializeRecyclerView(){
        SearchAdapter postsAdapter=new SearchAdapter(searchModels,this);
        recyclerViewForSearch.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewForSearch.setItemAnimator(new DefaultItemAnimator());
        recyclerViewForSearch.addItemDecoration(new Space(10));
        recyclerViewForSearch.setAdapter(postsAdapter);
    }

    @Override
    public void onClick(int position) {
        share=searchModels.get(position);
        Intent in=new Intent(getActivity(), ItemDetail.class);
        in.putExtra("type",2);
        startActivity(in);
    }
}