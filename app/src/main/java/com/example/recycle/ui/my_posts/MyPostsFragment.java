package com.example.recycle.ui.my_posts;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recycle.R;
import com.example.recycle.RetrofitInterface;
import com.example.recycle.Space;
import com.example.recycle.adapter.PostsAdapter;
import com.example.recycle.model.ResponseLoginModel;
import com.example.recycle.model.posts.PostsModel;
import com.example.recycle.model.posts.PostsModelResponse;
import com.example.recycle.ui.edit_post.EditPostFragment;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MyPostsFragment extends Fragment implements PostsAdapter.ItemListener {
    String BASE_URL="https://virutal.000webhostapp.com/RecycleAPI/ManageAccount/";
    Retrofit retrofit;
    RetrofitInterface retrofitInterface;
    SharedPreferences sharedPreferences;
    RecyclerView recyclerViewForPosts;
    ProgressBar progressBar;
    List<PostsModel> postsModelList;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_posts, container, false);
        recyclerViewForPosts = (RecyclerView) root.findViewById(R.id.postsRecyclerView);
        progressBar=(ProgressBar) root.findViewById(R.id.progressBarMyPost);
        retrofit = new Retrofit.Builder().
                baseUrl(BASE_URL).
                addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        ;
        retrofitInterface = retrofit.create(RetrofitInterface.class);
        sharedPreferences = getActivity().getSharedPreferences("myuser", Context.MODE_PRIVATE);
        HashMap<String,Integer> map=new HashMap<>();
        map.put("id_user",sharedPreferences.getInt("userId",-18));
        Call<PostsModelResponse> call=retrofitInterface.executeGetMyPosts(map);
        call.enqueue(new Callback<PostsModelResponse>() {
            @Override
            public void onResponse(Call<PostsModelResponse> call, Response<PostsModelResponse> response) {
                if(response.code()==200&&response.body().getStatus()==1){
                    postsModelList=response.body().getPosts();
                    initializeRecyclerView();
                    progressBar.setVisibility(View.INVISIBLE);
                }else {
                    Toast.makeText(getActivity(), response.body().getAlert(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onFailure(Call<PostsModelResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
            }
        });

        return root;
    }
    void initializeRecyclerView(){
        PostsAdapter postsAdapter=new PostsAdapter(postsModelList,this);
        recyclerViewForPosts.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewForPosts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewForPosts.addItemDecoration(new Space(10));
        recyclerViewForPosts.setAdapter(postsAdapter);
    }

    @Override
    public void onUpdateClick(View v, int position) {
        EditPostFragment editPostFragment=new EditPostFragment(postsModelList.get(position).getId());
        getFragmentManager().beginTransaction().replace(R.id.nav_host_fragment_content_main,editPostFragment).commit();

    }

    @Override
    public void onDeleteClick(View v, int position) {
        AlertDialog.Builder builder
                = new AlertDialog.Builder(getActivity());
        builder.setTitle("Do you want to delete this item?");
        View alertDialog = getLayoutInflater().inflate(R.layout.alert_dialog_style, null);
        builder.setView(alertDialog);
        Button confirm=alertDialog.findViewById(R.id.alertDialogConfirm);
        Button cancel=alertDialog.findViewById(R.id.alertDialogCancel);
        AlertDialog dialog=builder.create();
        dialog.show();
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                HashMap<String,Integer> map=new HashMap<>();
                map.put("id_user",sharedPreferences.getInt("userId",-18));
                map.put("item_id",postsModelList.get(position).getId());
                Call<ResponseLoginModel> call=retrofitInterface.executeDeletePost(map);
                call.enqueue(new Callback<ResponseLoginModel>() {
                    @Override
                    public void onResponse(Call<ResponseLoginModel> call, Response<ResponseLoginModel> response) {
                        if(response.code()==200&& response.body().getStatus()==1){
                            postsModelList.remove(position);
                            initializeRecyclerView();
                            Toast.makeText(getActivity(),"deleted",Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseLoginModel> call, Throwable t) {
                        Toast.makeText(getActivity(),t.toString(),Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }
}