package com.example.recycle;

import com.example.recycle.model.category.CategoryModelResponse;
import com.example.recycle.model.ResponseLoginModel;
import com.example.recycle.model.item_detail.ItemDetailModel;
import com.example.recycle.model.item_of_category.ItemOfCategoryResponse;
import com.example.recycle.model.my_orders.MyOrdersModelResponse;
import com.example.recycle.model.posts.PostsModelResponse;
import com.example.recycle.model.request.RequestsModelResponse;
import com.example.recycle.model.search_model.SearchModelResponse;
import com.example.recycle.ui.item_of_category.ItemOfCategory;

import org.json.JSONObject;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface RetrofitInterface {
    @FormUrlEncoded
    @POST("apiLogin.php")
    Call<ResponseLoginModel> executeLogin(@Field("Email_user") String email,@Field("Password_user")String password);

    @FormUrlEncoded
    @POST("apiRegister.php")
    Call<ResponseLoginModel> executeSignup(@Field("Name_user") String name,@Field("Email_user") String email,@Field("Password_user")String password,@Field("UserRoleId") int roleId,@Field("L_latitude") String latitude,@Field("L_longitude") String longitude);

    @FormUrlEncoded
    @POST("apiUpdateProfile.php")
    Call<ResponseLoginModel> executeEditProfile(@Field("id_user") int id,@Field("Email_user") String email, @Field("Password_user") String password, @Field("Name_user") String name, @Field("L_latitude") String latitude, @Field("L_longitude") String longitude);

    @GET("AllCategories.php")
    Call<CategoryModelResponse> executeGetCategory(@Query("id_user") int id);

    @GET
    Call<ResponseBody> executeGetImage(@Url String url);

    @POST("itemsOfCategory.php")
    Call<ItemOfCategoryResponse> executeGetItemOfCategory(@Body HashMap<String,Integer> body);

    @Multipart
    @POST("InsertItem.php")
    Call<ResponseLoginModel>executeAddItem(@Part("id_user") int id, @Part("item_name")String name, @Part("item_quantity")int quantity, @Part("item_details") String details, @Part("id_category") int cat_id, @Part("item_price")int price,@Part MultipartBody.Part image);

    @POST("detailsOfItem.php")
    Call<ItemDetailModel> executeGetItemDetail(@Body HashMap<String,Integer> body);

    @POST("myposts.php")
    Call<PostsModelResponse> executeGetMyPosts(@Body HashMap<String,Integer> body);

    @POST("deleteItem.php")
    Call<ResponseLoginModel> executeDeletePost(@Body HashMap<String,Integer> body);

    @Multipart
    @POST("updateItem.php")
    Call<ResponseLoginModel>executeEditPost(@Part("id_user") int id,@Part("item_id") int itemId, @Part("item_name")String name, @Part("item_quantity")int quantity, @Part("item_details") String details, @Part("id_category") int cat_id, @Part("item_price")int price,@Part MultipartBody.Part image);

    @POST("myrequest.php")
    Call<RequestsModelResponse>executeGetMyRequests(@Body HashMap<String,Integer> body);

    @POST("buyproducts.php")
    Call<ResponseLoginModel> executeBuyProducts (@Body HashMap<String,Integer> body);

    @POST("AcceptOrRefuse.php")
    Call<ResponseLoginModel> executeAcceptOrRefuse(@Body HashMap<String,Object> body);

    @POST("myorders.php")
    Call<MyOrdersModelResponse> executeGetMyOrders(@Body HashMap<String,Integer> body);

    @POST("deleteOrder.php")
    Call<ResponseLoginModel> executeDeleteOrder(@Body HashMap<String,Integer> body);

    @POST("searchOfItem.php")
    Call<SearchModelResponse> executeSearch(@Body HashMap<String,Object> body);
}
